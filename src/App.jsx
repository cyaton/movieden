import {useCallback, useState} from "react";

import Box from "./components/Box.jsx";
import ErrorMessage from "./components/ErrorMessage.jsx";
import Loader from "./components/Loader.jsx";
import Main from "./components/Main.jsx";
import MovieDetails from "./components/MovieDetails.jsx";
import MovieList from "./components/MovieList.jsx";
import Navbar from "./components/Navbar.jsx";
import NumResults from "./components/NumResults.jsx";
import Search from "./components/Search.jsx";
import WatchedMoviesList from "./components/WatchedMoviesList.jsx";
import WatchedSummary from "./components/WatchedSummary.jsx";
import {useMovies} from "./useMovies.js";
import {useLocalStorageState} from "./useLocalStorageState.js";


export default function App() {
    const [query, setQuery] = useState("");
    const [watched, setWatched] = useLocalStorageState([], "watched");
    const [selectedId, setSelectedId] = useState(null);
    const handleCloseMovie = useCallback(() => setSelectedId(null), [])
    const {movies, isLoading, error} = useMovies(query, handleCloseMovie);

    function handleSelectMovie(id) {
        setSelectedId((selectedId) => id === selectedId ? null : id)
    }

    function handleAddWatched(movie) {
        setWatched((watched) => [...watched, movie]);
    }

    function handleDeleteWatched(id) {
        setWatched(watched => watched.filter(movie => movie.imdbID !== id))
    }

    return (
        <>
            <Navbar>
                <Search query={query} setQuery={setQuery}/>
                <NumResults movies={movies}/>
            </Navbar>
            <Main>
                <Box>
                    {isLoading && <Loader/>}
                    {!isLoading && !error && <MovieList movies={movies} onSelectMovie={handleSelectMovie}/>}
                    {error && <ErrorMessage message={error}/>}
                </Box>
                <Box>
                    {
                        selectedId
                            ? <MovieDetails selectedId={selectedId} onCloseMovie={handleCloseMovie}
                                            onAddWatched={handleAddWatched} watched={watched}/>
                            : <div><WatchedSummary watched={watched}/>
                                <WatchedMoviesList watched={watched} onDeleteWatched={handleDeleteWatched}/>
                            </div>
                    }
                </Box>
            </Main>
        </>
    );
}



