export default function Logo() {
    return (
        <div className="logo">
            <span role="image">🎬</span>
            <h1>movieDen</h1>
        </div>
    )
}