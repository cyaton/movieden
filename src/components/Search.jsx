import {useEffect, useRef} from "react";
import {useKey} from "../useKey.js";

export default function Search({query, setQuery}) {
    const inputEl = useRef(null);

    // Focuses on searchbar when page is rendered
    useKey('Enter', function () {
        if (document.activeElement === inputEl.current) return;
        inputEl.current.focus();
        setQuery("");
    })

    return (
        <input
            className="search"
            type="text"
            placeholder="Search movies..."
            value={query}
            onChange={(e) => setQuery(e.target.value)}
            ref={inputEl}
        />
    )
}
