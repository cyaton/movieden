import {useState, useEffect, useRef} from "react";
import ErrorMessage from "./ErrorMessage.jsx";
import Loader from "./Loader.jsx";
import StarRating from "../StarRating.jsx";
import {useKey} from "../useKey.js";

export default function MovieDetails({selectedId, onCloseMovie, onAddWatched, watched}) {
    const [movie, setMovie] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState('')
    const [userRating, setUserRating] = useState('');

    const countRef = useRef(0);

    useEffect(function() {
        if (userRating) countRef.current++;
    }, [userRating]);

    const isWatched = watched.some(el => el.imdbID === movie.imdbID)
    const watchedUserrating = watched.find(movie => movie.imdbID === selectedId)?.userRating
    const {
        Title: title,
        Year: year,
        Poster: poster,
        Runtime: runtime,
        imdbRating,
        Plot: plot,
        Released: released,
        Actors: actors,
        Director: director,
        Genre: genre

    } = movie;

    const [avgRating, setAvgRating] = useState(0);

    function handleAdd() {
        const newWatchedMovie = {
            imdbID: selectedId,
            title,
            year,
            poster,
            imdbRating: Number(imdbRating),
            runtime: Number(runtime.split(' ').at(0)),
            userRating,
            countRatingDecisions: countRef.current,
        };

        onAddWatched(newWatchedMovie);
        onCloseMovie();

    }

    useKey('Escape', onCloseMovie)

    useEffect(function () {
        async function getMovieDetails() {

            try {
                setIsLoading(true);
                setError('');
                const res = await fetch(`http://www.omdbapi.com/?apikey=${import.meta.env.VITE_KEY}&i=${selectedId}`);

                if (!res.ok) throw new Error("Something went wrong with fetching movie details");

                const data = await res.json()

                if (data.Response === 'False') throw new Error("Movie details not found");

                setMovie(data);
            } catch (err) {
                console.log(err.message);
                if (err.name !== "AbortError") {
                    setError(err.message);
                }
            } finally {
                setIsLoading(false);
            }
        }

        getMovieDetails();
    }, [selectedId]);

    // Get the movie title to the page title
    useEffect(function () {
        if (!title) return;
        document.title = `movieDen ${title}`

        return function () {
            document.title = 'movieDen'
        }
    }, [title]);

    return (
        <div className="details">
            {isLoading && <Loader/>}
            {
                !isLoading && !error &&
                <>
                    <header>
                        <button className="btn-back" onClick={onCloseMovie}>&larr;</button>
                        <img src={poster} alt={`Poster of ${title}`}/>
                        <div className="details-overview">
                            <h2>{title}</h2>
                            <p>{released} &bull; {runtime}</p>
                            <p>{genre}</p>
                            <p><span>⭐️</span> {imdbRating} IMDb rating</p>
                        </div>
                    </header>

                    <section>
                        <p><em>{plot}</em></p>
                        <p>Starring {actors}</p>
                        <p>Directed by {director}</p>
                        <div className="rating">
                            {!isWatched ? (
                                <>
                                    <StarRating maxRating={10} size={24} onSetRating={setUserRating}/>
                                    {userRating > 0
                                        && (<button className="btn-add" onClick={handleAdd}>+ Add to list</button>)
                                    }{" "}
                                </>
                            ) : (

                                <p>You added this movie and rated it {watchedUserrating} ⭐️</p>
                            )
                            }
                            {error && <ErrorMessage message={error}/>}
                        </div>
                    </section>
                </>
            }
        </div>
    )
}
