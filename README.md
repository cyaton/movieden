# movieDen

## Description

movieDen is the code base for the web application found at [movieDen](https://gitlab.com/cyaton/movieden).

The app

- searches for movies in the [OMDb API](https://www.omdbapi.com/) and lets the user save a list of watched movies in the browser session.
- is built as a showcase project.
- does practice the use of the Reactjs most common hooks like `useState` or `useEffect` and custom hooks
- Saves the list of watched movies with rating in local storage

## Installation

movieDen runs in most modern browsers with JavaScript enabled.

## Usage

- Visit [moovie-den.netlify.app](https://moovie-den.netlify.app/) to see the app life in action
- Just type a movie name into the search bar and check the movie details in the list found
- While inspecting the details, the movie can be rated and added to a list of watched movies.
- Press Enter to enter the search bar anytime
- Press Escape to close the current detail
- When not knowing the exact movie name, make sure to make use of an `*` an asterisk in the search term. For example, `Interview*`

### Privacy

- The app is just meant to showcase and does not further security measures
- Nevertheless, the app only saves a list of the users rated movies in the local browser session

## Support

Feel free to contact me on [GitLab](https://gitlab.com/cyaton).

## Feedback

Any feedback or suggestions are welcome!

## Roadmap

The project is finished as of now

## Authors and acknowledgment

Contributors: Philipp Montazem

### Acknowledgments

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

#### Jonas Schmedtmann

Thanks to [Jonas Schmedtmann](https://codingheroes.io/)and his team for inspiring this project with the legendary [Ultimate React Course 2024](https://www.udemy.com/course/the-ultimate-react-course/?couponCode=REACT-LAUNCH)!

## LICENSE

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Active development. Readme last updated on February 14th, 2024
